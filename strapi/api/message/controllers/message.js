'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

 const { sanitizeEntity } = require('strapi-utils');

module.exports = {    
    
    create: async (ctx) => {
        const body = ctx.request.body
        const nameContact = body.name
        const sendTo = body.email
        const emailSubject = body.subject
        const emailMessage = body.message       

        try{
            const emailOptions = {
                to: sendTo,
                name: nameContact,
                subject: `${emailSubject}`,
                html: `from <strong>${nameContact}</strong> <br/><br/> <hr/> message: <br/> ${emailMessage}`,
            }
            await strapi.plugins['email'].services.email.send(emailOptions);             
            ctx.send({message: 'Email sent'})
        }catch(err) {
            strapi.log.error(`Error sending email to ${sendTo}`)
            ctx.send({error: 'Error sending email'})
        }

        const entity = await strapi.services.message.create(ctx.request.body);

        return sanitizeEntity(entity, { model: strapi.models.message });
    }   
    
};