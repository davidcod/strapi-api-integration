import React, {useEffect, useState} from 'react'
import axios from 'axios'
import { Label, InputText, Padded } from '@buffetjs/core';
import { Wrapper} from "@googlemaps/react-wrapper";

import MapContainer from '../MapContainer'
import Marker from '../Marker';


const MapWrapper = (props) => {    
  const [mapData, setMapData] = useState()
  const [location, setLocation] = useState({})
  const [markers, setMarkers] = useState([]);
  //const [markerData, setMarkerData] = useState();

  const updateMapData = (dataMapValue) =>{    
    props.onChange({target: {name: 'map', value: dataMapValue}});
  }

  // const Marker = (options) => {
    
  //   useEffect(() => {
  //     if (!markerData) {
  //       setMarkerData(new window.google.maps.Marker());
  //     }
  
  //     return () => {
  //       if (markerData) {
  //         markerData.setMap(null);
  //       }
  //     };
  //   }, [markerData]);
  
  //   useEffect(() => {
  //     if (markerData) {
  //       const infowindow = new window.google.maps.InfoWindow({
  //         content: 'Salt Lake City'
  //       });
         
  //       markerData.setOptions(options);
  
  //       markerData.addListener("click", () => {
  //         infowindow.open({
  //           anchor: marker,
  //           shouldFocus: false
  //         });
  //       });
  //     }
  //   }, [markerData, options]);
  
  //   return null;
  // };

  useEffect(()=>{
    if(!props.value) {      
      updateMapData(mapData)
    }
  },[]); 

  useEffect(() =>{
    axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${'copacabana'}&key=AIzaSyBCQKbDYtZGhLaDWDNZFda32dNpPFWyNJc`)
    .then(function(response){
      setMarkers([response.data.results[0].geometry.location]);
    })
  },[])

  useEffect(()=>{
    if(location) {
      setLocation(location)
    }
  },[location])

  const handleChangeData = (e) =>{
    updateMapData(mapData)
    setMapData(e.target.value);   
  };

  const handleKeyPress = (e) =>{
    if(e.key==="Enter") {
      e.preventDefault()
      updateMapData(mapData)
    }
  }

  //console.log("DEBUG ONCHANGE:", handleChangeData)

  return (
    <Wrapper apiKey={"AIzaSyBCQKbDYtZGhLaDWDNZFda32dNpPFWyNJc"}>
      <Padded bottom size="md">
        <Label htmlFor="address">Address Store</Label>
        <InputText
          mapData={()=>setMapData(e.target.value)}
          onKeyPress={()=>updateMapData()}      
          name="address"
          onChange={handleChangeData}
          placeholder="Address here..."
          type="text"
          value={mapData}
        />
      </Padded>
      <MapContainer
        center={markers.length ? markers[0] : {lat: 40.7765233,lng: -112.06057}}
        zoom={8}
        style={{ flexGrow: "1", height: "500px" }}
      >
        {markers.map((marker, key) => { 
          return <Marker key={key} position={marker} /> 
        })}
      </MapContainer>
    </Wrapper>
  )
}

export default MapWrapper