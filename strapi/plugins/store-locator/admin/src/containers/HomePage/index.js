import React, { memo, useState } from 'react';
import styled from 'styled-components'
import { Header } from '@buffetjs/custom';
import { Flex, InputText, Select } from '@buffetjs/core';

const Wrapper = styled.div`
  padding: 24px;
`

import Map from '../../components/Map'

const HomePage = () => {
 
  return (
    <Wrapper>
    <Header title={{label: 'Map'}} />
    <Map />
    </Wrapper>
  );
};

export default memo(HomePage);
