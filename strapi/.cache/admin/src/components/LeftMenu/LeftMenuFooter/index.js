/**
 *
 * LeftMenuFooter
 *
 */

import React from 'react';

import Wrapper from './Wrapper';

function LeftMenuFooter({ version }) {

  return (
    <Wrapper>
      <div className="poweredBy">
        <a key="website" href="https://cpduxy.com" target="_blank" rel="noopener noreferrer">
          Powered by Coduxy
        </a>
      </div>

    </Wrapper>
  );
}

export default LeftMenuFooter;
