import React from 'react';
import LogoStrapi from '../../../../assets/images/logo-coduxy-dark.svg';
import Img from './Img';

const Logo = () => <Img src={LogoStrapi} alt="Coduxy" />;

export default Logo;
